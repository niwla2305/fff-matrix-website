module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        'fff-green-dark': '#1B7340',
        'fff-green-light': '#1DA64A',
        'fff-blue': '#90D3ED'
      },
      backgroundImage: theme => ({
        'earth': "url('/img/erde_hell_no_bg.svg')",
      }),
      animation: {
        'spin-once': 'spin 1s linear',
      }
    },
    fontFamily: {
      'header': "Jost-600",
      "text": "Jost-400"
    }
  },
  variants: {},
  plugins: [],
}
